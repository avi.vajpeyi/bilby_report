# Bilby Report

This package can assist in creating a webpage summarising multiple bilby result files.
An example of a multi-result summary page can be seen [here](https://ldas-jobs.ligo.caltech.edu/~avi.vajpeyi/bcr_discriminator/software_injections/injections_in_gaussiannoise/IMRPhenompV2/multi_run_summary/summary_report.html).


## Installation
To install, clone the repo, cd into it, and run `python setup.py develop` (develop as currently it is still in active development).

## Usage
Once you install, you can run the following to make the webpage 
`bilby_report --res <path/to/results> --web <path/to/web/dir> --prior <path/to/pe/prior/file>`


A better way to use this might be to [inherit](https://www.w3schools.com/python/python_inheritance.asp) the [BilbyResultsPage](https://git.ligo.org/avi.vajpeyi/bilby_report/blob/master/multiple_run_summary/bilby_results_summary_page.py) 
class and adjust the `get_sections` method... 

You might also have to edit the `evid_dataframe`.