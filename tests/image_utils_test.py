import os
import shutil
import unittest

from bilby_report.tools import file_utils, image_utils


class ImageTesterCase(unittest.TestCase):
    def setUp(self) -> None:
        self.root_path = "tests/test_files"
        self.outdir = "test_out"
        self.re = "test_orig.png"
        os.makedirs(self.outdir, exist_ok=True)

    def tearDown(self) -> None:

        if os.path.exists(self.outdir):
            shutil.rmtree(self.outdir)

    def test_get_image(self):
        filepaths = file_utils.get_filepaths(self.root_path, self.re)
        self.assertTrue(len(filepaths) == 1)

    def copy_images_for_testing(self):
        for f in file_utils.get_filepaths(self.root_path, self.re):
            final_path = os.path.join(self.outdir, os.path.basename(f))
            shutil.copyfile(src=f, dst=final_path)

    def test_image_text_addition(self):
        self.copy_images_for_testing()
        filepaths = file_utils.get_filepaths(self.outdir, self.re)
        image_utils.add_text_to_images(["sample text"], filepaths)
        for f in filepaths:
            self.assertTrue(os.path.exists(f))

    def test_gif_creation(self):
        self.copy_images_for_testing()
        filepaths = file_utils.get_filepaths(self.outdir, self.re)
        gif_path = os.path.join(self.outdir, "test.gif")
        image_utils.make_gif(gif_save_path=gif_path, image_paths=filepaths)
        self.assertTrue(os.path.exists(gif_path))
        image_utils.make_gif(
            gif_save_path=gif_path, image_paths=filepaths, text_list=["top right text"]
        )
        self.assertTrue(os.path.exists(gif_path))


if __name__ == "__main__":
    unittest.main()
