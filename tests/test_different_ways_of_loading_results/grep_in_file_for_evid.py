filepath = (
    "tests/test_files/sample_results/test_H1_data20_100-20_injection1_result.json"
)

import io
import mmap
import re
import timeit

import bilby

WHOLE_FILE = 0


def get_number_from_string(string):
    return re.search(r"\d*\.?\d+", string).group()


def get_z_from_pythonic_grep(pattern=b"log_evidence(.*)", filepath=filepath):
    with io.open(filepath, "r", encoding="utf-8") as f:
        mem_map = mmap.mmap(f.fileno(), WHOLE_FILE, access=mmap.ACCESS_READ)
        evid_line = re.search(pattern, mem_map).group()
        evid_line = str(evid_line, "utf-8")
        num = get_number_from_string(evid_line)
        return num


def get_z_from_bilby_result(filepath=filepath):
    res = bilby.core.result.read_in_result(filepath)
    return res.log_evidence


start = timeit.timeit()
get_z_from_pythonic_grep()
end = timeit.timeit()
print(f"grep {end - start}")


start = timeit.timeit()
get_z_from_bilby_result()
end = timeit.timeit()
print(f"bilby {end - start}")
