filepath = (
    "tests/test_files/sample_results/test_H1_data20_100-20_injection1_result.json"
)

import io
import mmap
import re

WHOLE_FILE = 0


def get_number_from_string(string):
    return re.search(r"\d*\.?\d+", string).group()


def pythonic_grep(pattern=b"log_evidence(.*)", file_path=filepath):
    with io.open(file_path, "r", encoding="utf-8") as f:
        mem_map = mmap.mmap(f.fileno(), WHOLE_FILE, access=mmap.ACCESS_READ)
        evid_line = re.search(pattern, mem_map).group()
        evid_line = str(evid_line, "utf-8")
        num = get_number_from_string(evid_line)
        return num


print(pythonic_grep())
