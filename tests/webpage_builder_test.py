import os
import unittest

import numpy as np
import pandas as pd
from bilby_report import (
    DataframeTemplate,
    IFrameTemplate,
    PriorTemplate,
    SummaryTemplate,
)


class TestMultiPageGenerator(unittest.TestCase):
    def setUp(self) -> None:
        self.webdir = "out_test"
        self.test_img_path = "tests/test_files/test.png"
        self.test_prior = "tests/test_files/test.prior"

    def tearDown(self) -> None:
        import shutil

        if os.path.exists(self.webdir):
            shutil.rmtree(self.webdir)

    def test_full_page(self):
        save_dir = self.webdir

        num_rows = 15
        num_columns = 20
        df = pd.DataFrame(
            np.random.randint(0, 100, size=(num_rows, num_columns)),
            columns=["col{}".format(i) for i in range(num_columns)],
        )

        # building summary page
        sections = [
            IFrameTemplate(
                title="Image Test",
                content_path=self.test_img_path,
                height="500",
                width="100%",
                save_dir=self.webdir,
            ),
            PriorTemplate(
                title="Prior Test", prior_filepath=self.test_prior, height="500"
            ),
            DataframeTemplate(
                title="Other Dataframe Test",
                dataframe=df,
                height="500",
                text="{} rows".format(len(df)),
            ),
        ]
        summary_page = SummaryTemplate(title="Test Page", sections=sections)
        summary_page.render_to_file(save_dir)
        self.assertTrue(os.path.exists(os.path.join(save_dir, "summary_report.html")))

    def test_section_render(self):
        section = IFrameTemplate(
            title="Image Test",
            content_path=self.test_img_path,
            height="500",
            width="90%",
            save_dir=self.webdir,
        )
        self.assertIsInstance(section.render(), str)
