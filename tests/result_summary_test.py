import os
import shutil
import unittest

import pandas as pd
from bilby_report.summary_templates import summary_extractor


class ResultSummaryTest(unittest.TestCase):
    def setUp(self) -> None:
        self.outdir = "tests/outtest/"
        os.makedirs(self.outdir, exist_ok=True)
        self.outfile = os.path.join(self.outdir, "evidences.csv")
        self.results_dir = "tests/test_files/sample_results"

    def tearDown(self) -> None:
        if os.path.exists(self.outdir):
            shutil.rmtree(self.outdir)

    def test_output(self):
        summary_extractor.output_result_summary_file(
            res_directory=self.results_dir, out_filename=self.outfile
        )
        self.assertTrue(
            os.path.exists(self.outfile), "Make sure that the summary outdir made"
        )

        # checking contents of file
        df = pd.read_csv(self.outfile)
        self.assertIsInstance(df, pd.DataFrame)
        self.assertIsNotNone(df.lnZs.values)
        self.assertIsNotNone(df.walltime_H1.values)
        self.assertIsNotNone(df.walltime_L1.values)
        self.assertIsNotNone(df.walltime_H1L1.values)


if __name__ == "__main__":
    unittest.main()
