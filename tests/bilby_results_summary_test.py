import os
import shutil
import unittest

import pandas as pd
from bilby_report import main
from bilby_report.summary_templates.injection_summary import InjectionSummary


class BilbyResultsSummary(unittest.TestCase):
    def setUp(self):
        self.outdir = "tests/outtest"
        self.webdir = os.path.join(self.outdir, "webdir")
        self.results_dir = "tests/test_files/sample_results"
        self.prior = "tests/test_files/test.prior"
        self.true_evid_csv = "tests/test_files/true_evidences.csv"
        self.true_evid = pd.read_csv(self.true_evid_csv)
        os.makedirs(self.outdir, exist_ok=True)
        os.makedirs(self.webdir, exist_ok=True)

    def tearDown(self) -> None:
        if os.path.exists(self.outdir):
            shutil.rmtree(self.outdir)

    def test_arg_parser(self):
        parsed_args = main.webpage_parse_args(
            [
                "--res",
                self.outdir,
                "--web",
                self.webdir,
                "--prior",
                self.prior,
                "--scratch",
            ]
        )
        self.assertEqual(parsed_args.res, self.outdir)
        self.assertEqual(parsed_args.scratch, True)
        self.assertEqual(parsed_args.title, "Results Summary")

        parsed_args = main.webpage_parse_args(
            [
                "--res",
                self.outdir,
                "--web",
                self.webdir,
                "--prior",
                self.prior,
                "--title",
                "Some Title",
                "--scratch",
            ]
        )
        self.assertEqual(parsed_args.title, "Some Title")

    def test_generate_bilby_results(self):
        """Extract results and make summary page"""
        main.BilbyResultsPage(
            webdir=os.path.join(self.webdir),
            results_dir=self.results_dir,
            prior_filepath=self.prior,
            create_from_scratch=True,
        ).render()

        # comparing quality of extracted evid
        evid_path = os.path.join(self.webdir, "multi_run_summary/evidences.csv")
        extracted_evid = pd.read_csv(evid_path)
        self.assertAlmostEqual(
            self.true_evid.lnZs.values, extracted_evid.lnZs.values, delta=0.01
        )

    def test_generate_bilby_results_injection(self):
        report_path = os.path.join(
            self.webdir, "inj/multi_run_summary/summary_report.html"
        )
        main.BilbyResultsPage(
            webdir=os.path.join(self.webdir, "inj"),
            results_dir=self.results_dir,
            prior_filepath=self.prior,
            create_from_scratch=True,
            result_summary=InjectionSummary,
        ).render()
        self.assertTrue(os.path.isfile(report_path))


if __name__ == "__main__":
    unittest.main()
