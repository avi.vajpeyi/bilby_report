import glob
import os
import shutil
import unittest

import pandas as pd
from bilby_report.summary_templates.summary_extractor import (
    combine_multiple_summary_files,
)


class CombineMultipleSummary_test(unittest.TestCase):
    def setUp(self):
        self.outdir = "test"
        os.makedirs(self.outdir, exist_ok=True)
        self.res_dir = "tests/test_files/small_evid_combiner/"
        self.file_regex = "foreground_*.csv"

    def tearDown(self):
        if os.path.exists(self.outdir):
            shutil.rmtree(self.outdir)

    def test_combiner(self):
        outfn = os.path.join(self.outdir, "evid.csv")
        combine_multiple_summary_files(
            res_directory=self.res_dir, out_filename=outfn, file_regex=self.file_regex,
        )
        num_res = len(glob.glob(os.path.join(self.res_dir, self.file_regex)))
        num_rows = len(pd.read_csv(outfn))
        self.assertEqual(num_res, num_rows)


if __name__ == "__main__":
    unittest.main()
