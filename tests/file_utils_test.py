import unittest

from bilby_report.tools import file_utils


class FileUtilsTesterCase(unittest.TestCase):
    def test_get_filepaths(self):
        regex = "*/*/*.prior"
        filepaths = file_utils.get_filepaths(root_path=".", file_regex=regex)
        self.assertTrue(len(filepaths) == 1)

    def test_filter_list(self):
        lst = ["file1.txt", "file2.png", "file3.hdf"]
        regex = "(.*)png"
        filter_list = file_utils.filter_list(lst=lst, filter_regex=regex)
        self.assertTrue(len(filter_list) == 1)

    def test_gps_from_filename(self):
        gps_time = 1000.3123
        gps_str = str(gps_time).replace(".", "-")
        filename = f"label_H1L1_dataXX_{gps_str}_morelabel_result.json"
        self.assertEqual(file_utils.find_gps_time(filepath=filename), gps_time)

    def test_data_idx_from_filename(self):
        data_idxs = [21, 1000, 0, 1]
        for data_idx in data_idxs:
            filename = f"label_H1L1_data{data_idx}_1000-21_morelabel_result.json"
            self.assertEqual(file_utils.find_data_idx(filepath=filename), data_idx)


if __name__ == "__main__":
    unittest.main()
