from setuptools import setup

MAIN_EXE = "bilby_report"
EXTRACTION_EXE = "bilby_extract_results"
COMBINE_EXE = "bilby_combine_results"

setup(
    name="bilby_report",
    version="0.1",
    description="A module to help build summary pages",
    author="Avi Vajpeyi",
    author_email="avi.vajpeyi@gmail.com",
    packages=[
        "bilby_report",
        "bilby_report.html_templates",
        "bilby_report.resources",
        "bilby_report.summary_templates",
        "bilby_report.templates",
        "bilby_report.tools",
    ],
    package_data={
        "bilby_report.html_templates": [
            "*.html"
        ],  # All html files from html_templates folder
        "bilby_report.resources": [
            "*.ttf"
        ],  # All text fonts from from resources folder
    },
    install_requires=[
        "jinja2>=2.10.1",
        "pandas>=0.25.0",
        "tqdm",
        "bilby>=0.5.4",
        "matplotlib>=3.1.1",
        "plotly>=4.0.0",
        "seaborn",
        "numpy>=1.17.0",
        "pillow",
        "pytest-cov",
        "coverage-badge",
        "gwpy>=0.15.0",
    ],
    # external
    # packages as
    # dependencies
    entry_points={
        "console_scripts": [
            f"{MAIN_EXE}=bilby_report.main:main",
            "bilby_injection_report=bilby_report.main:injection_main",
            f"{EXTRACTION_EXE}=bilby_report.main:extract_results",
            f"{COMBINE_EXE}=bilby_report.main:combine_results",
        ]
    },
)
