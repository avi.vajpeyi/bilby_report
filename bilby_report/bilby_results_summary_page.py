import logging
import os
from typing import Optional

import pandas as pd

from . import (
    DataframeTemplate,
    IFrameTemplate,
    ImageTemplate,
    PriorTemplate,
    SummaryTemplate,
)
from .summary_templates.result_summary import ResultSummary
from .summary_templates.summary_extractor import output_result_summary_file
from .tools import file_utils, parameter_keys, plotting
from .tools.latex_label import LATEX_LABEL_DICT


class BilbyResultsPage(object):
    def __init__(
        self,
        webdir: str,
        results_dir: str,
        prior_filepath: str,
        title: Optional[str] = "Results Summary",
        result_summary: Optional[ResultSummary] = ResultSummary,
        create_from_scratch: Optional[bool] = True,
    ):
        self.result_summary = result_summary
        self.title = title
        self.create_from_scratch = create_from_scratch
        self.webdir_content_dir_name = "multi_run_summary"
        self.webdir = webdir
        self.results_dir = results_dir
        self.prior_filepath = prior_filepath
        logging.info(
            "Trying to locate result files from {}...".format(self.results_dir)
        )
        self.evid_file_path = os.path.join(self.webdir, "evidences.csv")

    @property
    def webdir(self):
        return self.__webdir

    @webdir.setter
    def webdir(self, path):
        path = os.path.join(path, self.webdir_content_dir_name)
        if self.create_new(path):
            os.makedirs(path, exist_ok=True)
        self.__webdir = path

    @property
    def pp_plot_path(self):
        return self.__pp_plot_path

    @pp_plot_path.setter
    def pp_plot_path(self, path):
        if self.create_new(path):
            logging.info(
                "PP-setup.cfg of result data from {}...".format(self.results_dir)
            )
            plotting.plot_pp_test(
                result_files=file_utils.get_coherent_result_files(self.results_dir),
                filename=path,
            )
        self.__pp_plot_path = path

    @property
    def evid_file_path(self):
        return self.__evid_file_path

    @evid_file_path.setter
    def evid_file_path(self, path):
        if self.create_new(path):
            logging.info("Extracting result data from {}...".format(self.results_dir))
            output_result_summary_file(
                res_directory=self.results_dir, out_filename=path
            )
        self.__evid_file_path = path

    def create_new(self, path):
        should_create_new = self.create_from_scratch or os.path.exists(path)
        logging.info(
            "{} {} being created.".format(path, "is" if should_create_new else "is not")
        )
        return should_create_new

    def get_sections(self):

        logging.info("Loading results data from {}...".format(self.evid_file_path))
        evid_dataframe = pd.read_csv(self.evid_file_path)
        sections = [
            PriorTemplate(
                title="Prior file for results",
                prior_filepath=self.prior_filepath,
                height="500",
            ),
            DataframeTemplate(
                title="Evidences", dataframe=evid_dataframe, height="500", text=""
            ),
        ]

        # Additional plots (REALLY UGLY HACKY CODE)
        try:
            self.pp_plot_path = os.path.join(self.webdir, "pp_test.png")
            sections.append(
                ImageTemplate(
                    title="PP-Test",
                    img_path=self.pp_plot_path,
                    width="100%",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping pp-test: {}".format(e))

        try:
            mass_scatter_path = plotting.plot_mass_scatter(
                evid_dataframe, filename=os.path.join(self.webdir, "mass_scatter.html")
            )
            sections.append(
                IFrameTemplate(
                    title="Masses",
                    content_path=mass_scatter_path,
                    height="500",
                    width="90%",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping plotting of injected masses: {}".format(e))

        try:
            mass_distribution_path = plotting.plot_dataframe_coloumn_histograms(
                evid_dataframe,
                plot_key_to_label={
                    parameter_keys.MASS_RATIO: LATEX_LABEL_DICT[
                        parameter_keys.MASS_RATIO
                    ],
                    parameter_keys.CHIRP_MASS: LATEX_LABEL_DICT[
                        parameter_keys.CHIRP_MASS
                    ],
                },
                filename=os.path.join(self.webdir, "mass_distribution.html"),
            )
            sections.append(
                IFrameTemplate(
                    title="",
                    content_path=mass_distribution_path,
                    height="500",
                    width="90%",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping plotting of mass distribution: {}".format(e))

        try:
            analysis_stats_path = plotting.plot_dataframe_coloumn_histograms(
                evid_dataframe,
                plot_key_to_label={
                    parameter_keys.SNR: LATEX_LABEL_DICT[parameter_keys.SNR],
                    parameter_keys.LOG_BF: LATEX_LABEL_DICT[parameter_keys.LOG_BF],
                    parameter_keys.LOG_BCR: LATEX_LABEL_DICT[parameter_keys.LOG_BCR],
                },
                filename=os.path.join(self.webdir, "analysis_histograms.html"),
            )
            sections.append(
                IFrameTemplate(
                    title="PE Statistics",
                    content_path=analysis_stats_path,
                    height="500",
                    save_dir=self.webdir,
                )
            )
        except Exception as e:
            logging.warning("Skipping plotting of PE stats: {}".format(e))

        return sections

    def render(self):
        summary_page = SummaryTemplate(title=self.title, sections=self.get_sections())
        summary_page.render_to_file(self.webdir)
