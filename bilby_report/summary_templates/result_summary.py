import json
import logging
from typing import List, Optional

import corner
import numpy as np
import pandas as pd
from tqdm import tqdm

from ..tools import calculations, parameter_keys as keys
from ..tools.file_utils import find_data_idx, find_gps_time, read_in_result

ALPHA = 1e-6
BETA = 1e-4

NUMBER_OF_POSTERIOR_SAMPLES = 500


class ResultSummary(object):
    def __init__(self, combined_result_file: str, get_samples: Optional[bool] = False):

        combined_result = read_in_result(combined_result_file)
        if not combined_result:
            raise OSError(f"Error reading {combined_result_file}")

        self.log_zs = combined_result.log_evidence
        self.log_bf = self.log_zs - combined_result.log_noise_evidence

        self.log_zg = {}
        self.log_zn = {}
        self.log_bf_i = {}
        self.nsamples = {}
        self.walltime = {}
        self.posterior_stats = summarise_posterior_samples(combined_result.posterior)

        self.nsamples.update(self.get_num_samples_dict(keys.H1L1, combined_result))
        self.walltime.update(self.get_walltime(keys.H1L1, combined_result))

        for d in keys.DETECTORS:
            d_result = read_in_result(
                combined_result_file.replace("_H1L1_", "_{}_".format(d))
            )
            if not d_result:
                raise OSError(f"Error reading {combined_result_file}")
            log_zg_i = d_result.log_evidence
            log_zn_i = d_result.log_noise_evidence

            log_bf_i = log_zg_i - log_zn_i

            self.log_zg.update({d: log_zg_i})
            self.log_zn.update({d: log_zn_i})
            self.log_bf_i.update({d: log_bf_i})
            self.nsamples.update(self.get_num_samples_dict(d, d_result))
            self.walltime.update(self.get_walltime(d, d_result))

        self.path = combined_result_file

        try:
            if not isinstance(combined_result.posterior, type(None)) and get_samples:
                self.posterior = combined_result.posterior
        except ValueError:
            raise OSError(f"No posteriors in {combined_result_file}")

    @property
    def gps_time(self):
        return find_gps_time(filepath=self.path)

    @property
    def data_idx(self):
        return find_data_idx(filepath=self.path)

    @property
    def log_bcr(self):
        try:
            log_bcr = calculations.get_log_bcr(
                log_zs=self.log_zs,
                log_zg_h=self.log_zg["H1"],
                log_zg_l=self.log_zg["L1"],
                log_zn_h=self.log_zn["H1"],
                log_zn_l=self.log_zn["L1"],
                alpha=ALPHA,
                beta=BETA,
            )

        except (AttributeError, TypeError):
            log_bcr = None

        return log_bcr

    def get_evidence_dict(self):
        evid_dict = {
            keys.LOG_ZS: self.log_zs,
            keys.LOG_BF: self.log_bf,
            keys.LOG_BCR: self.log_bcr,
        }

        for d in keys.DETECTORS:
            evid_dict.update(
                {
                    keys.LOG_ZG_D.format(d): self.log_zg[d],
                    keys.LOG_ZN_D.format(d): self.log_zn[d],
                    keys.LOG_BF_D.format(d): self.log_bf_i[d],
                }
            )
        return evid_dict

    @property
    def posterior(self):
        return self.__posterior

    @posterior.setter
    def posterior(self, df):
        """
        Downsample the posterior samples
        :param df:
        :return:
        """
        if len(df) >= NUMBER_OF_POSTERIOR_SAMPLES:
            self.__posterior = df.sample(n=NUMBER_OF_POSTERIOR_SAMPLES)
        else:
            self.__posterior = df

    def get_meta_data_dict(self):
        meta_data_dict = {}
        meta_data_dict.update(**self.walltime)
        meta_data_dict.update(**self.nsamples)
        return meta_data_dict

    @staticmethod
    def get_num_samples_dict(key, result):
        return {keys.NUM_SAMPLES.format(key): len(result.posterior)}

    @staticmethod
    def get_walltime(key, result):
        """Units: hrs"""
        return {keys.WALL_TIME.format(key): result.sampling_time / 3600}

    def to_dict(self):
        my_dict = {keys.GPS_TIME: self.gps_time, keys.DATA_IDX: self.data_idx}
        evid_dict = self.get_evidence_dict()
        meta_data_dict = self.get_meta_data_dict()
        paths_dict = {
            keys.H1L1_RESULT_FILE: self.path,
            keys.H1_RESULT_FILE: self.path.replace("_H1L1_", "_H1_"),
            keys.L1_RESULT_FILE: self.path.replace("_H1L1_", "_L1_"),
        }
        my_dict.update(**evid_dict)
        my_dict.update(**meta_data_dict)
        my_dict.update(**paths_dict)
        my_dict.update(**self.posterior_stats)
        return my_dict


def get_result_summaries(combined_result_files):
    result_summaries = []
    for f in tqdm(combined_result_files):
        try:
            result_summaries.append(ResultSummary(f))
        except (json.decoder.JSONDecodeError, OSError, AttributeError) as e:
            logging.error(f"ERROR: {e}")
            result_summaries.append(f)
        logging.info("Bilby results info stored into a list")
    return result_summaries


@np.vectorize
def get_result_summary_dict_list(result_summary: ResultSummary):
    return result_summary.to_dict()


def summarise_posterior_samples(
        posterior: pd.DataFrame,
        quantiles: Optional[List[float]] = [0.16, 0.84]
):
    summary = {}
    for param in posterior.columns.values:
        samples = posterior[param]
        qtles = corner.quantile(x=posterior[param], q=quantiles)
        summary[f"{param}_lower"] = qtles[0]
        summary[f"{param}_upper"] = qtles[1]
        summary[param] = np.median(samples)
    return summary