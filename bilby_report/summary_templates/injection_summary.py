from ..tools import conversions, file_utils, parameter_keys as keys
from .result_summary import ResultSummary


class InjectionSummary(ResultSummary):
    def __init__(self, combined_result_file: str):
        super(InjectionSummary, self).__init__(combined_result_file)

        # Injection data
        self.inj_num = file_utils.find_injection_number(combined_result_file)
        combined_result = file_utils.read_in_result(combined_result_file)
        self.truths = file_utils.flatten_dict(combined_result.injection_parameters)
        self.truths = conversions.set_mass1_mass2_from_mchirp_q(self.truths)
        self.snr = conversions.get_snr_from_metadata(combined_result.meta_data)

    def to_dict(self):
        result_summary_dict = {keys.INJECTION_NUMBER: self.inj_num, keys.SNR: self.snr}
        result_summary_dict.update(self.get_evidence_dict())
        if self.truths:  # this unwraps the injected parameters
            result_summary_dict.update(self.truths)
        return result_summary_dict
