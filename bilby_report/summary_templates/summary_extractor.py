import datetime
import logging
import os
import time
from typing import Optional

import numpy as np
import pandas as pd
import tqdm

from ..tools import file_utils
from .result_summary import get_result_summaries, get_result_summary_dict_list


def output_result_summary_file(
    out_filename: str,
    res_directory: Optional[str] = None,
    coherent_file_regex: Optional[str] = file_utils.H1L1_RESULT_FILE,
    coherent_file: Optional[str] = None,
):
    """ Opens up the result files with regex in the res_directory specified and saves a
    csv with summary data into a file.

    :param res_directory: dir with the result files (a recursive search performed in dir)
    :param out_filename: the filename of the csv where the result data stored
    :param coherent_file_regex: the regex to search for
    :return:
    """

    # Get result filepaths
    if coherent_file:
        result_fnames = [coherent_file]
    else:
        result_fnames = file_utils.get_filepaths(
            res_directory, file_regex=coherent_file_regex
        )

    # Extract the results into a list
    logging.info(f"Extracting {len(result_fnames)} bilby results")
    t = time.process_time()
    parsed_results = get_result_summaries(result_fnames)
    elapsed_time = time.process_time() - t
    logging.info(
        f"Extracting results took " f"{str(datetime.timedelta(seconds=elapsed_time))}."
    )

    # Log missing files
    if len(parsed_results) < len(result_fnames):
        missing_idx = [
            i for i, val in enumerate(parsed_results) if isinstance(val, str)
        ]
        missing_results = list(np.array(parsed_results)[missing_idx])
        log_failed_files(result_fnames, missing_results)

    # Store extracted files
    result_idx = [i for i, val in enumerate(parsed_results) if not isinstance(val, str)]
    remaining_results = list(np.array(parsed_results)[result_idx])
    if remaining_results:
        evid = get_result_summary_dict_list(remaining_results)
        evid = file_utils.list_dicts_to_dict_lists(evid)
        data = pd.DataFrame.from_dict(evid)
        data.to_csv(out_filename)
        logging.info(f"Saved extracted results into {out_filename}")
    else:
        logging.info("No extracted results to save")


def log_failed_files(result_files, failed_files):
    num_files = len(result_files)
    num_failed = len(failed_files)
    percent_fail = round(num_failed / num_files * 100, 2)
    logging.info(
        "{}% ({}/{}) result extractions failed. ".format(
            percent_fail, num_failed, num_files
        )
    )

    for f in failed_files:
        logging.warning("Ignored {}".format(f))


def get_evid_file_contents(f):
    with open(f, "rb") as infile:
        data = infile.readlines()
        if len(data) < 2:
            raise ValueError("File does not have enough contents")
        return data


def combine_multiple_summary_files(
    res_directory: str,
    out_filename: str,
    file_regex: Optional[str] = file_utils.H1L1_RESULT_FILE,
):
    # Get result filepaths
    mini_fnames = file_utils.get_filepaths(res_directory, file_regex)
    failed_files = []

    # Write contents to an outfile
    outdir = os.path.dirname(out_filename)
    if os.path.isdir(outdir) is False:
        logging.info(f"Creating {outdir} to store the combined file in.")
        os.makedirs(outdir)
    with open(out_filename, "wb") as outfile:
        for f_id, f in enumerate(tqdm.tqdm(mini_fnames)):
            try:
                data = get_evid_file_contents(f)
                if f_id == 0:
                    outfile.write(data[0])
                outfile.write(data[1])
            except Exception:
                failed_files.append(f)

    if os.path.exists(out_filename):
        logging.info(f"Combined evidences written in: {out_filename}")
    else:
        logging.info(f"No combined evidence file created")

    if len(failed_files) > 0:
        logging.warning(
            f"Could not extract data from {len(failed_files)} files:\n"
            f"{failed_files}"
        )