""" Generates HTML table of a pandas dataframe
"""

import os
from typing import Optional

from ..tools.file_utils import copy_file_to_savedir
from .template_base import HTMLTemplate
from .template_paths import IFRAME_HTML, get_html_template


class IFrameTemplate(HTMLTemplate):
    def __init__(
        self,
        content_path: str,
        save_dir: str,
        title: str,
        width: Optional[str] = "100%",
        height: Optional[str] = "100%",
        text: Optional[str] = "",
    ):
        self.title = title
        self.width = width
        self.height = height
        self.text = text
        self.content_path = copy_file_to_savedir(
            source_path=os.path.abspath(content_path), destination_dir=save_dir
        )

    def render(self) -> str:
        return self.html.render(
            title=self.title,
            width=self.width,
            height=self.height,
            content_path=os.path.basename(self.content_path),
            text=self.text,
        )

    @property
    def html(self) -> str:
        return get_html_template(IFRAME_HTML)
