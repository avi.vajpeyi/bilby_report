""" Generates HTML table of a pandas dataframe
"""
from typing import Optional

import pandas as pd

from .iframe_template import HTMLTemplate
from .template_paths import TABLE_HTML, get_html_template


class DataframeTemplate(HTMLTemplate):
    def __init__(
        self,
        dataframe: pd.DataFrame,
        title: str,
        height: Optional[str] = "100%",
        text: Optional[str] = "",
        has_link=False,
    ):
        self.title = title
        self.height = height
        self.text = text
        dataframe = remove_unnamed_cols(dataframe)
        # to prevent dataframe string truncation
        old_width = pd.get_option("display.max_colwidth")
        pd.set_option("display.max_colwidth", -1)
        self.dataframe_html = dataframe.to_html(
            classes="display nowrap", table_id="", border=0, justify="left"
        )
        pd.set_option("display.max_colwidth", old_width)
        self.has_link = has_link

    def render(self) -> str:
        html_string = self.html.render(
            title=self.title,
            height=self.height,
            table_content=self.dataframe_html,
            text=self.text,
        )

        if self.has_link:
            html_string = html_string.replace("&lt;", "<")
            html_string = html_string.replace("&gt;", ">")

        return html_string

    @property
    def html(self) -> str:
        return get_html_template(TABLE_HTML)


def remove_unnamed_cols(df):
    return df.loc[:, ~df.columns.str.contains("^Unnamed")]
