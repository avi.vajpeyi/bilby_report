""" Generates HTML table of a prior file
"""

from typing import Optional

import pandas as pd
from bilby.core.prior import PriorDict

from .dataframe_template import DataframeTemplate


class PriorTemplate(DataframeTemplate):
    def __init__(
        self,
        prior_filepath: str,
        title: str,
        height: Optional[str] = "100%",
        text: Optional[str] = "",
    ):
        prior_dataframe = prior_to_dataframe(prior_filepath)
        super(PriorTemplate, self).__init__(
            dataframe=prior_dataframe,
            title=title,
            height=height,
            text=text,
        )


def prior_to_dataframe(prior_filepath):
    prior_dict = dict(PriorDict(prior_filepath))
    prior_dict = {k: str(v) for k, v in prior_dict.items()}
    prior_keys = [key for key in prior_dict.keys()]
    prior_values = [value for value in prior_dict.values()]
    prior_dict = {"Parameters": prior_keys, "Prior": prior_values}
    prior_dataframe = pd.DataFrame.from_dict(prior_dict)
    return prior_dataframe
