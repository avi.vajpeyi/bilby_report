import os

from jinja2 import Template

IFRAME_HTML = "iframe_template.html"
SECTION_IMG_HTML = "section_image.html"
MULTI_RUN_HTML = "multi_result_template.html"
TABLE_HTML = "table_template.html"
IMAGE_HTML = "image_template.html"


def get_html_template(template):
    root = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.join(root, "../html_templates")
    template_html = os.path.join(templates_dir, template)
    template_filereader = open(template_html)
    template_string = template_filereader.read()
    template_filereader.close()
    template = Template(template_string)
    return template
