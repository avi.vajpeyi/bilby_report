import os
from typing import List

from . import links
from .iframe_template import IFrameTemplate
from .template_base import HTMLTemplate
from .template_paths import MULTI_RUN_HTML, get_html_template


class SummaryTemplate(HTMLTemplate):
    def __init__(self, title: str, sections: List[IFrameTemplate]):
        self.title = title
        self.sections = sections

    def render(self) -> str:
        return self.html.render(
            stylesheet=links.STYLESHEET,
            documentation_link=links.BILBY_DOCS,
            repo_link=links.BILBY_REPO,
            logo_link=links.BILBY_LOGO,
            title=self.title,
            sections=self.sections,
        )

    def render_to_file(self, save_dir) -> None:
        html_code = self.render()
        report_file_name = os.path.join(save_dir, "summary_report.html")
        with open(report_file_name, "w") as report_file:
            report_file.write(html_code)
        report_file.close()

    @property
    def html(self) -> str:
        return get_html_template(MULTI_RUN_HTML)
