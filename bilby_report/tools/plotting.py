import logging
import os
import random
from collections.abc import Iterable
from typing import List, Optional

import matplotlib
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
import seaborn as sns
from bilby.core.result import make_pp_plot
from gwpy.time import from_gps
from matplotlib import pyplot as plt
from plotly import graph_objs as go, tools as plotly_tools
from plotly.offline import plot
from plotly.subplots import make_subplots
from tqdm import tqdm

from . import file_utils, parameter_keys
from .latex_label import LATEX_LABEL_DICT

sns.set(font_scale=1.5)
sns.axes_style("white")
sns.set_style("ticks", {"font.family": "serif"})

font = {"weight": "normal", "size": 22}

matplotlib.rc("font", **font)
plt.rcParams["axes.grid"] = True
plt.rcParams["xtick.direction"] = "in"
plt.rcParams["ytick.direction"] = "in"
plt.rcParams["xtick.top"] = True
plt.rcParams["ytick.right"] = True

random.seed(0)

DEFAULT_PLOTLY_COLORS = [
    "rgb(31, 119, 180)",
    "rgb(255, 127, 14)",
    "rgb(44, 160, 44)",
    "rgb(214, 39, 40)",
    "rgb(148, 103, 189)",
    "rgb(140, 86, 75)",
    "rgb(227, 119, 194)",
    "rgb(127, 127, 127)",
    "rgb(188, 189, 34)",
    "rgb(23, 190, 207)",
]

AXIS_STYLE = dict(
    showgrid=True,
    showline=True,
    linewidth=2,
    linecolor="black",
    ticks="inside",
    tickwidth=2,
    mirror="allticks",
    title_font_size=20,
)

# plot formatting
kwargs = {
    "back": {
        "label": "Background",
        "color": "gray",
        "marker": ".",  # (5, 2),
        "lw": 3,
        "alpha": 0.7,
    },
    "swinj": {
        "label": "SW injections",
        "color": "dodgerblue",
        "markeredgecolor": "none",
        "lw": 2,
        "marker": "h",
        "alpha": 0.4,
    },
    "hwinj": {
        "label": "HW injections",
        "color": "lightgreen",
        "lw": 2,
        "markeredgecolor": "none",
        "marker": "s",
        "markersize": 8,
        "alpha": 0.8,
    },
    "S190521g": {
        "label": "S190521g",
        "color": "crimson",
        "marker": "*",
        "markersize": 15,
        "markeredgecolor": "None",
        "lw": 3,
    },
    "line": {"color": "crimson", "alpha": 0.5, "lw": 2},
    "legend": {"loc": "upper right", "numpoints": 1, "fontsize": 15, "framealpha": 0.2},
}
kwargs.update(
    dict(
        background=kwargs["back"], injection=kwargs["swinj"], foreground=kwargs["swinj"]
    )
)
fg = kwargs["foreground"].update(dict(label="Foreground", color="orange"))
kwargs.update(dict(foreground=fg))


def get_plotly_colour(i):
    num = len(DEFAULT_PLOTLY_COLORS)
    return DEFAULT_PLOTLY_COLORS[i % num]


def plot_pp_test(result_files: List[str], filename: str, keys: Optional[dict] = None):
    """ Saves a pp-setup.cfg plot based on the result file list provided
    :param result_files: list of strings of the filepaths
    :param filename: location where the pp-plot is saved
    :param keys: A list of keys to use, if None defaults to search_parameter_keys
    :return: None
    """
    logging.info("Plotting {}".format(filename))
    results = file_utils.read_in_results(result_files)
    results_with_posteriors = file_utils.check_results_for_posteriors(results)
    filtered_results = list(filter(None, results_with_posteriors))
    if filtered_results:
        fig, pvals = make_pp_plot(results, save=False)
        fig.savefig(filename, dpi=500)
        logging.info("pp-test plot saved at {} with pvals: {}".format(filename, pvals))
    else:
        logging.info("pp-test skipped")


def plot_mass_scatter(df: pd.DataFrame, filename="mass_scatter.html", title=None):
    """ Scatterplot of the masses
    :param df:
    :param filename:
    :param title:
    :return:
    """
    logging.info("Plotting {}".format(filename))
    text = []
    for i in range(len(df)):
        text_i = "({:.2f}, {:.2f}) q={:.2f}".format(
            df[parameter_keys.MASS_2].values[i],
            df[parameter_keys.MASS_1].values[i],
            df[parameter_keys.MASS_RATIO].values[i],
        )
        if parameter_keys.INJECTION_NUMBER in df.columns:
            inj_num = "Inj {}".format(df[parameter_keys.INJECTION_NUMBER].values[i])
            text_i = ": ".join([inj_num, text_i])
        text.append(text_i)

    mass_scat_trace = go.Scatter(
        x=df[parameter_keys.MASS_2],
        y=df[parameter_keys.MASS_1],
        text=text,
        mode="markers",
        marker=dict(
            color=df[parameter_keys.MASS_RATIO],  # set color equal to a variable
            colorscale="Viridis",
            showscale=True,
            colorbar=dict(title="q"),
            cmax=1,
            cmin=0,
        ),
        hoverinfo="text",
        name="$M_2 \\text{ vs } M_1$",
    )

    # Edit the layout
    layout = dict(
        title=title,
        xaxis=dict(title=LATEX_LABEL_DICT[parameter_keys.MASS_1], **AXIS_STYLE),
        yaxis=dict(title=LATEX_LABEL_DICT[parameter_keys.MASS_2], **AXIS_STYLE),
        showlegend=False,
        font=dict(size=15),
    )
    fig = dict(data=[mass_scat_trace], layout=layout)
    graph_url = plot(fig, filename=filename, auto_open=False, include_mathjax="cdn")
    return os.path.abspath(graph_url)


def plot_dataframe_coloumn_histograms(
    df: pd.DataFrame, plot_key_to_label: dict, filename="statistics.html"
):
    """ Histograms of dataframe columns
    :param df: dataframe containing data to be histogrammed
    :param filename: name of the plot
    :param plot_key_to_label: the keys of the dataframe to be histogrammed : labels
    :return: the path to the saved file
    """
    logging.info("Plotting {}".format(filename))
    num_plots = len(plot_key_to_label)
    fig = make_subplots(rows=1, cols=num_plots, shared_yaxes=True)
    fig["layout"].update(showlegend=False, font=dict(size=15))

    for idx, (plot_key, plot_label) in enumerate(plot_key_to_label.items()):
        hist_trace = go.Histogram(
            x=df[plot_key], name=plot_key, opacity=0.75, histnorm="probability"
        )
        fig.add_trace(hist_trace, row=1, col=idx + 1)
        cur_setting = AXIS_STYLE.copy()
        cur_setting.update(dict(row=1, col=idx + 1))
        fig.update_xaxes(title_text=plot_label, **cur_setting)
        fig.update_yaxes(**cur_setting)
    y_axis_settings = AXIS_STYLE.copy()
    y_axis_settings.update(
        dict(title_text="Probability Density", showticklabels=True, row=1)
    )
    fig.update_yaxes(col=1, **y_axis_settings)
    fig.update_yaxes(col=num_plots, side="right", **y_axis_settings)
    graph_url = plot(fig, filename=filename, auto_open=False, include_mathjax="cdn")

    return os.path.abspath(graph_url)


def plot_times(times_dataframe, filename="times.html", start_time=None, end_time=None):
    """ Plots the start end end times of the interferometers
    :param times_dataframe: dataframe with H1, L1 t0 and t1 times
    :param filename: filename of the plot
    :param start_time: start time of the chunk (can be None)
    :param end_time: end time of the chunk (can be None)
    :return:
    """
    logging.info("Plotting {}".format(filename))
    df = times_dataframe
    logging.info("Converting GPS-->datetimes")
    idx = df.index.values
    h1_t0 = np.vectorize(from_gps)(df["H1_t0"].values)
    h1_t1 = np.vectorize(from_gps)(df["H1_t1"].values)
    l1_t0 = np.vectorize(from_gps)(df["L1_t0"].values)
    l1_t1 = np.vectorize(from_gps)(df["L1_t1"].values)

    logging.info("Converting datetimes-->traces")
    h1_traces = get_time_trace(h1_t0, h1_t1, idx)
    l1_traces = get_time_trace(l1_t0, l1_t1, idx)
    num_traces = len(idx)

    logging.info("Plotting datetimes")
    fig = make_subplots(rows=2, cols=1, shared_xaxes=True, vertical_spacing=0.02)
    for i in tqdm(range(num_traces)):
        fig.add_trace(h1_traces[i], row=1, col=1)
        fig.add_trace(l1_traces[i], row=2, col=1)

    xaxis2_range = {}
    if start_time and end_time:
        xaxis2_range = dict(range=[from_gps(start_time), from_gps(end_time)])

    frame_style = dict(mirror=True, showline=True, linewidth=2, linecolor="black")

    yaxis_style = dict(
        side="left",
        showgrid=False,
        zeroline=False,
        showticklabels=False,
        range=[0, 1],
        **frame_style,
    )
    fig["layout"].update(
        dict(
            yaxis=dict(title="H1 Trigger", **yaxis_style),
            yaxis2=dict(title="L1 Trigger", **yaxis_style),
            xaxis2=dict(title="Time", **xaxis2_range, **frame_style),
            showlegend=False,
            font=dict(size=15),
        )
    )

    graph_url = plot(fig, filename=filename, auto_open=False)

    return graph_url


@np.vectorize
def get_time_trace(t0, t1, idx):
    x = [t0] * 3 + [t1] * 3
    y = [0, 0.5, 1, 1, 0.5, 0]
    col = get_plotly_colour(idx)
    trace_style = dict(
        name="Trigger{}".format(idx),
        fill="tozeroy",
        fillcolor=col,
        line_color=col,
        hoveron="points+fills",
    )
    return go.Scatter(x=x, y=y, **trace_style)


def flatten(l):
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el


def plot_far_vs_rank(plot_data, rank_x_label, title="", log_scale=True, filename=""):
    """

    :param plot_data: List[dict(far, rank, plot_kwargs, )]
    :param rank_x_label: str of the rank's x axis label
    :param title:
    :return:
    """

    if not filename:
        filename = f"ln_far_vs_{rank_x_label.replace(' ', '_')}"
    plt.rc("text", usetex=False)

    fig = plt.figure(figsize=(8, 8))
    gs = gridspec.GridSpec(3, 3)

    # setup the main and x and y hist plots
    ax_main = plt.subplot(gs[1:3, :2])
    ax_xDist = plt.subplot(gs[0, :2], sharex=ax_main)
    ax_xCumDist = ax_xDist.twinx()
    ax_yDist = plt.subplot(gs[1:3, 2], sharey=ax_main)
    ax_yCumDist = ax_yDist.twiny()

    # axis settings
    ax_main.set_ylabel(r"ln PyCBC FAR (ln[yr$^{-1}$])", **font)
    ax_main.set_xlabel(rank_x_label, **font)
    ax_main.grid("on")
    ax_xDist.set(ylabel="count")
    ax_xCumDist.set(ylabel="cumulative")
    ax_yDist.set(xlabel="count")
    ax_yCumDist.set(xlabel="cumulative")

    for d in plot_data:
        x, y = d["rank"], d["far"]
        ax_main.plot(x, y, ls="None", **d["plot_kwargs"])
        c = d["plot_kwargs"]["color"]
        if d["plot_kwargs"]["label"] == "Foreground":
            for xi in x:
                ax_main.axvline(xi, color=c, alpha=0.2)
                ax_xDist.axvline(xi, color=c, alpha=0.2)
            for yi in y:
                ax_main.axhline(yi, color=c, alpha=0.2)
                ax_yDist.axhline(yi, color=c, alpha=0.2)
            pass
        else:
            # ax_xCumDist.hist(x, bins=100, cumulative=True, histtype='step', normed=True,
            #                  color=c, align='mid', alpha=0.3)
            ax_xDist.hist(x, bins=100, align="mid", color=c)
            # ax_yCumDist.hist(y, bins=100, cumulative=True, histtype='step', normed=True,
            #                  color=c, align='mid', orientation='horizontal', alpha=0.3)
            ax_yDist.hist(y, bins=100, orientation="horizontal", align="mid", color=c)

    # ax_main.axvline(1, **kwargs["line"])
    ax_main.legend(**kwargs["legend"])
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])

    if title:
        text = fig.suptitle(title)
        plt.savefig(filename, bbox_extra_artists=[text], bbox_inches="tight")
    else:
        plt.savefig(filename, bbox_inches="tight")

    return filename
