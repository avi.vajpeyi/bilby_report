import glob
import json
import logging
import os
import re
import shutil

import numpy as np
from bilby.core import result

L1_RESULT_FILE = "*_L1_*_result.json"
H1_RESULT_FILE = "*_H1_*_result.json"
H1L1_RESULT_FILE = "*_H1L1_*_result.json"
RESULT_FILE = "*_result.json"
INJ_NUM_REGEX = ".*injection(\\d+|$).*_result.json"
GPS_TIME_REGEX = r"\d+-\d+"
DATA_REGEX = r"_data(\d+)_"


def read_in_result(filename):
    try:
        logging.info(f"Reading {filename}")
        return result.read_in_result(filename)
    except (json.decoder.JSONDecodeError, OSError, ValueError) as e:
        logging.warning(f"Cannot read {filename}\nError: {e}.\nSkipping this file")
        return None


@np.vectorize
def read_in_results(filename):
    return read_in_result(filename)


@np.vectorize
def check_results_for_posteriors(result):
    """
    make sure that the result has a posterior
    :param result: bilby result
    :return: result if posterior present
    """
    if result:
        if not isinstance(result.posterior, type(None)):
            return result
    return None


def get_filepaths(root_path: str, file_regex: str):
    files_paths = glob.glob(os.path.join(root_path, file_regex))
    if len(files_paths) == 0:
        logging.warning(f"0 files with regex {file_regex} found in '{root_path}' ")
    return files_paths


def filter_list(lst, filter_regex: str):
    filter_results = filter(re.compile(filter_regex).search, lst)
    selected_files = list(filter_results)
    if not selected_files:
        logging.warning(f"0 files with regex {filter_regex} found in '{lst}' ")
    return selected_files


def list_dicts_to_dict_lists(lst: list):
    """convert list of dict to dict of lists"""
    converted_dict = {}
    if isinstance(lst, list) or isinstance(lst, np.ndarray):
        dict_keys = lst[0].keys()
        converted_dict = {key: [d.get(key, np.NaN) for d in lst] for key in dict_keys}
    else:
        logging.warning(f"Not a list : {type(lst)}")
    if not converted_dict:
        logging.warning(f"Could not convert list to dict: {lst}")
    return converted_dict


def flatten_dict(d: dict):
    if d:
        return {k: v.item(0) if isinstance(v, np.ndarray) else v for k, v in d.items()}
    else:
        return None


def find_injection_number(filepath):
    numbers_in_filepath = re.findall(re.compile(INJ_NUM_REGEX), filepath)
    if numbers_in_filepath:
        inj_num = int(numbers_in_filepath.pop())
    else:
        inj_num = -1
        logging.debug(
            f"Cant find inj number\n{filepath}, regexresult:{numbers_in_filepath}"
        )
    return inj_num


def find_gps_time(filepath):
    """ Gets GPS time from filepath
    :param filepath: file with a gps time formatted as 1122313-001231 (LHS are
    decimal values)
    :return: GPS time as float (6dp)
    """
    gps_pattern = re.compile(GPS_TIME_REGEX)
    gps_match = gps_pattern.findall(filepath)
    gps = np.NAN
    if gps_match:
        gps_string = gps_match.pop()
        gps_string = gps_string.replace("-", ".")
        num_dp = len(gps_string) - (gps_string.index(".") + 1)

        if num_dp != 6:
            logging.debug(
                f"GPS string {gps_string} has " f"{num_dp} dp instead of 6 dp"
            )

        gps = float(gps_string)
    else:
        logging.debug("GPS not found in path")

    return gps


def find_data_idx(filepath):
    """ Gets data idx from filepath
    :param filepath: file with a data idx formatted as dataXX where XX is some int
    :return: data idx (as int)
    """
    idx_pattern = re.compile(DATA_REGEX)
    idx_match = idx_pattern.findall(filepath)
    idx = np.NAN
    if idx_match:
        idx = int(idx_match.pop())
    else:
        logging.debug("GPS not found in path")
    return idx


def copy_file_to_savedir(source_path: str, destination_dir: str):
    # check source path and get filename from source
    validate_path(source_path)
    filename = os.path.basename(source_path)

    # check if the destination dir exists
    if not os.path.exists(destination_dir):
        os.makedirs(destination_dir, exist_ok=True)
    validate_dir(destination_dir)

    # move from source to final destination
    final_destination = os.path.join(destination_dir, filename)
    if os.path.abspath(final_destination) != source_path:
        shutil.copyfile(source_path, final_destination)
    return final_destination


def validate_dir(dir):
    if not os.path.isdir(dir):
        raise OSError("{} not a valid dir".format(dir))


def validate_path(path):
    if not os.path.exists(path):
        raise OSError("{} not a valid path".format(path))


def get_coherent_result_files(directory):
    return get_filepaths(directory, file_regex=H1L1_RESULT_FILE)
