import logging
import os

import tqdm
from PIL import Image, ImageDraw, ImageFont

logging.basicConfig(level=logging.INFO)

SQ_SIZE = 750

try:
    FONT = ImageFont.truetype("bilby_report/resources/FreeMono.ttf", 20)
except Exception:
    FONT = ""


def add_text_to_images(text_list, image_paths):
    total_len = len(text_list)
    logging.info(f"Adding text to images (eg {text_list[0]} to {image_paths[0]})")
    for p, text in tqdm.tqdm(zip(image_paths, text_list), total=total_len):
        img = Image.open(p)
        img = draw_text_on_image(text, img)
        img.save(p)


def draw_text_on_image(text, image):
    draw = ImageDraw.Draw(image)
    img_w, _ = image.size
    txt_w, _ = draw.textsize(text, FONT)
    location = (img_w - txt_w, 0)  # TOP LEFT
    color = "rgb(0,0,0)"  # BLACK color
    draw.text(location, text, color, font=FONT)
    return image


def resize_image(image, desired_size, pad=False):
    old_size = image.size  # old_size is in (width, height) format
    ratio = float(desired_size) / max(old_size)
    new_size = tuple([int(x * ratio) for x in old_size])
    # resize image
    resized_im = image.resize(new_size, Image.ANTIALIAS)
    if pad:
        # Add padding to image to make it a square image
        new_im = Image.new("RGB", (desired_size, desired_size), color="black")
        new_im.paste(
            resized_im,
            ((desired_size - new_size[0]) // 2, (desired_size - new_size[1]) // 2),
        )
    else:
        new_im = resized_im
    return new_im


def make_gif(image_paths, gif_save_path, duration=100, text_list=None):
    """
    save into a GIF file that loops forever
    :param image_paths: list of paths of the images
    :param gif_save_path: the save path of the gif
    :return: None
    """
    gif_dir = os.path.dirname(gif_save_path)
    if not os.path.isdir(gif_dir):
        os.mkdir(gif_dir)

    logging.info(f"Generating gif {gif_save_path}")
    frames = []
    for idx, p in tqdm.tqdm(enumerate(image_paths), total=len(image_paths)):
        im = Image.open(p)
        # make gif smaller otherwise gif will be too big in size
        im = resize_image(im, SQ_SIZE, pad=False)
        if text_list:
            im = draw_text_on_image(text=text_list[idx], image=im)
        frames.append(im)

    frames[0].save(
        gif_save_path,
        format="GIF",
        append_images=frames[1:],
        save_all=True,
        duration=duration,
        loop=0,
    )
