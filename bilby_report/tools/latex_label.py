from . import parameter_keys

# parameter_keys
LATEX_LABEL_DICT = {
    parameter_keys.MASS_1: "$M_1$",
    parameter_keys.MASS_2: "$M_2$",
    parameter_keys.CHIRP_MASS: "$M$",
    parameter_keys.MASS_RATIO: "$q$",
    parameter_keys.LUMINOSITY_DISTANCE: "$D_l$",
    parameter_keys.REDSHIFT: "$\\mathcal{z}$",
    parameter_keys.DEC: "$\\mathrm{DEC}$",
    parameter_keys.RA: "$\\mathrm{RA}$",
    parameter_keys.THETA_JN: "$\\theta_{JN}$",
    parameter_keys.PSI: "$\\psi$",
    parameter_keys.PHASE: "$\\phi$",
    parameter_keys.A_1: "$a_1$",
    parameter_keys.A_2: "$a_2$",
    parameter_keys.TILT_1: "$\\theta_1$",
    parameter_keys.TILT_2: "$\\theta_2$",
    parameter_keys.PHI_12: "$\\Delta\\phi$",
    parameter_keys.PHI_JL: "$\\phi_{JL}$",
    parameter_keys.INJECTION_NUMBER: "InjNum",
    parameter_keys.LOG_BF: "$\\log \\text{BF}$",
    parameter_keys.LOG_BCR: "$\\log \\text{BCR}$",
    parameter_keys.SNR: "$\\text{SNR}$",
    parameter_keys.URL: "Inj #",
}
