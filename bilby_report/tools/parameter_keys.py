MASS_1 = "mass_1"
MASS_2 = "mass_2"
GEOCENT_TIME = "geocent_time"
CHIRP_MASS = "chirp_mass"
MASS_RATIO = "mass_ratio"
LUMINOSITY_DISTANCE = "luminosity_distance"
REDSHIFT = "redshift"
DEC = "dec"
RA = "ra"
THETA_JN = "theta_jn"
PSI = "psi"
PHASE = "phase"
A_1 = "a_1"
A_2 = "a_2"
TILT_1 = "tilt_1"
TILT_2 = "tilt_2"
PHI_12 = "phi_12"
PHI_JL = "phi_jl"
SNR = "snr"
NUM_SAMPLES = "nsamples_{}"
WALL_TIME = "walltime_{}"
OPTIMAL_SNR = "optimal_snr_{}"

LOG_BF = "lnBF"
LOG_ZS = "lnZs"
LOG_BCR = "lnBCR"

LOG_BF_D = "lnBF_{}"
LOG_ZG_D = "lnZg_{}"
LOG_ZN_D = "lnZn_{}"

H1 = "H1"
L1 = "L1"
H1L1 = "H1L1"
DETECTORS = [H1, L1]
INTERFEROMETERS = "interferometers"
INJECTION = "injection"


INJECTION_NUMBER = "InjNum"


GPS_TIME = "gps_time"
DATA_IDX = "data_idx"
LIKELIHOOD = "likelihood"
MATCHED_FILTER_SNR = "matched_filter_SNR"
URL = "url"


H1_RESULT_FILE = "h1_result_file"
L1_RESULT_FILE = "l1_result_file"
H1L1_RESULT_FILE = "h1l1_result_file"
