import numpy as np

DETECTORS = ["H1", "L1"]


@np.vectorize
def get_log_bcr(log_zs, log_zg_h, log_zg_l, log_zn_h, log_zn_l, alpha, beta):
    """
    BCR = alpha * Zs  /   Prod_i [(beta * Zg_i) + ((1-beta) * Zn_i)]


    :param log_zs:
    :param log_zg_h:
    :param log_zg_l:
    :param log_zn_h:
    :param log_zn_l:
    :param alpha:
    :param beta:
    :return: log_bcr: float
    """

    try:
        log_zg = {"H1": log_zg_h, "L1": log_zg_l}
        log_zn = {"H1": log_zn_h, "L1": log_zn_l}

        log_alpha = np.log(alpha)
        log_beta = np.log(beta)
        with np.errstate(divide="ignore"):
            log_1_minus_beta = np.log(1 - beta)

        log_bcr_numerator = log_alpha + log_zs
        log_bcr_denominator = sum(
            [
                np.logaddexp(log_beta + log_zg[d], log_1_minus_beta + log_zn[d])
                for d in DETECTORS
            ]
        )
        log_bcr = log_bcr_numerator - log_bcr_denominator
    except RuntimeWarning:
        log_bcr = np.NaN
    return log_bcr
