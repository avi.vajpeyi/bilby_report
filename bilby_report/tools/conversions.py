"""
Code for conversions and data extraction.....
REALLY UGLY. FUNCTIONS SHOULD ONLY BE RETURNING ONE TYPE!! MAJOR CODESMELLS

TODO: use bilby's conversion functions instead of these conversion functions

"""


import math

import numpy as np

from .parameter_keys import CHIRP_MASS, MASS_1, MASS_2, MASS_RATIO


def get_q_from_mass1_mass2(data_dict):
    if data_dict.get(MASS_RATIO, None):
        return data_dict.get(MASS_RATIO)
    elif data_dict.get(MASS_1, None) and data_dict.get(MASS_2, None):
        data_dict.get(MASS_1) / data_dict.get(MASS_2)
    else:
        None


def set_mass1_mass2_from_mchirp_q(param: dict):
    if param:
        if {MASS_1, MASS_2}.issubset(set(param)):
            q = param[MASS_RATIO]
            mchirp = param[CHIRP_MASS]
            param[MASS_1] = (q ** (2.0 / 5.0)) * ((1.0 + q) ** (1.0 / 5.0)) * mchirp
            param[MASS_2] = (q ** (-3.0 / 5.0)) * ((1.0 + q) ** (1.0 / 5.0)) * mchirp
        return param
    else:
        return None


def get_snr_from_metadata(meta_data: dict) -> float:
    try:
        interferometer_data = meta_data.get("likelihood").get("interferometers")
        snr_vals = np.array(
            [
                interferometer_data.get(interferometer_id).get("matched_filter_SNR")
                for interferometer_id in ["H1", "L1"]
            ]
        )
        return math.sqrt(sum(abs(snr_vals) ** 2))
    except (AttributeError, TypeError):
        return None
