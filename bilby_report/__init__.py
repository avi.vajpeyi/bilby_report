from .templates.iframe_template import IFrameTemplate
from .templates.dataframe_template import DataframeTemplate
from .templates.prior_template import PriorTemplate
from .templates.summary_template import SummaryTemplate
from .templates.image_template import ImageTemplate
