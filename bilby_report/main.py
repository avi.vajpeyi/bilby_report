import argparse
import logging
import os
import sys

from .bilby_results_summary_page import BilbyResultsPage, output_result_summary_file
from .summary_templates.summary_extractor import combine_multiple_summary_files
from .tools import file_utils
from .tools.timeout import timeout

logging.basicConfig(level=logging.INFO)


def webpage_parse_args(inputted_args):
    parser = argparse.ArgumentParser(description="Get evidences from bilby result")
    parser.add_argument("--res")
    parser.add_argument("--web")
    parser.add_argument("--prior", default=".")
    parser.add_argument("--title", default="Results Summary")
    parser.add_argument("--scratch", action="store_true", default=True)
    args = parser.parse_args(inputted_args)
    return args


def main():
    args = webpage_parse_args(sys.argv[1:])
    file_utils.validate_path(args.prior)
    webpage = BilbyResultsPage(
        webdir=args.web,
        results_dir=args.res,
        prior_filepath=args.prior,
        title=args.title,
        create_from_scratch=args.scratch,
    )
    webpage.render()


def result_extraction_parse_args(inputted_args):
    parser = argparse.ArgumentParser(description="Get evidences from bilby result")
    parser.add_argument("--res")
    parser.add_argument("--web", default=None)
    parser.add_argument("--fname", default=None)
    parser.add_argument("--regex", default=None)
    parser.add_argument("--coherent-result", default=None)
    args = parser.parse_args(inputted_args)
    return args


@timeout(seconds=120)
def extract_results():
    """
    bilby_extract_results --res <path to results dir> --web <path to webdir>

    OR

    bilby_extract_results --res <path to results dir> --regex <coherent regex to us> --web <path to webdir>

    :return: none
    """
    args = result_extraction_parse_args(sys.argv[1:])
    logging.info(f"Extract results: {args}")
    if args.fname and args.coherent_result and not args.web:
        output_result_summary_file(
            out_filename=args.fname, coherent_file=args.coherent_result
        )
    elif args.fname and args.regex and not args.web:
        output_result_summary_file(
            res_directory=args.res,
            out_filename=args.fname,
            coherent_file_regex=args.regex,
        )
    else:
        os.makedirs(args.web, exist_ok=True)
        output_result_summary_file(
            res_directory=args.res, out_filename=os.path.join(args.web, "evidences.csv")
        )


def combine_results():
    args = result_extraction_parse_args(sys.argv[1:])
    logging.info(f"Combining results: {args}")
    combine_multiple_summary_files(
        res_directory=args.res, out_filename=args.fname, file_regex=args.regex,
    )
